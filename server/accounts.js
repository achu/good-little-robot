/**
* Accounts configuration
*/
import { TITLE } from '../imports/constants.js'
import { EMAIL } from '../imports/constants.js'

// General settings
Accounts.emailTemplates.siteName = TITLE
Accounts.emailTemplates.from = `${TITLE} <${EMAIL}>`


// Enrolment
Accounts.urls.enrollAccount = (token) => {
    return Meteor.absoluteUrl("enroll-account/" + token);
}

Accounts.emailTemplates.enrollAccount.subject = (user) => {
  return `[${TITLE}] An account has been created for you`
}

Accounts.emailTemplates.enrollAccount.text = (user, url) => {
  return `${user.profile.name},\n\n`
      + `An account has been created for you to log your hours.\n`
      + `Click the link below to set your password:\n\n`
      + url
}

// Reset password
Accounts.urls.resetPassword = (token) => {
    return Meteor.absoluteUrl("reset-password/" + token);
}

Accounts.emailTemplates.resetPassword.subject = (user) => {
  return `[${TITLE}] Reset your password`
}

Accounts.emailTemplates.resetPassword.text = (user, url) => {
  return `${user.profile.name},\n\n`
      + `Click the link below to reset your password:\n\n`
      + url
}
