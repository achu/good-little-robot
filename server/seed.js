/**
* Seed database with initial account
*/

import { Person } from '../imports/api/person.js'

if (Person.find().count() === 0) {

  Accounts.createUser({
    email: 'admin@local',
    password: 'asdf',
    profile: {
      name: 'Admin [UPDATE ME!]',
      source: 'Local',
      isAdmin: true
    }
  })

}
