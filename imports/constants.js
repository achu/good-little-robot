/**
 * Application constants/config
 */

// Application title
export const TITLE = "Good Little Robot"
// Administrator email account
export const EMAIL = "support@scimentis.com"
