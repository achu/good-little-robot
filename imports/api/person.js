import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'
import { check } from 'meteor/check'

export const Person = Meteor.users

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('people', () => {
    return Person.find()
  })
}

if (Meteor.isServer) {
  Meteor.methods({
    /**
    * Create a new user
    * @param  {JSON object} object All fields for new user (minus password)
    */
    'person.insert'(object) {
      // Check that input is valid
      const pattern = {
        name: String,
        email: String,
        team: Match.Maybe(String),
        rate: Match.Maybe(Number),
        isAdmin: Boolean,
        source: String
      }
      check(object, pattern)

      // Insert into DB if administrator
      if (!Meteor.user() || !Meteor.user().profile.isAdmin) {
        throw new Meteor.Error('not-authorized','Must be administrator to add a person')
      }
      const userId = Accounts.createUser({
        email: object.email,
        profile: {
          name: object.name,
          team: object.teamId,
          rate: object.rate,
          source: object.source,
          isAdmin: object.isAdmin
        }
      })

      // Send enrollment email to set password
      Accounts.sendEnrollmentEmail(userId)
    },
    /**
    * Removes user
    * @param  {String} personId User record ID
    */
    'person.remove'(personId) {
      check(personId, String)
      Person.remove(personId)
    },
    /**
    * Updates user email
    * @param  {String} personId User record ID
    * @param  {String} email    New user email address
    */
    'person.update.email'(personId, email) {
      check(personId, String)
      check(email, String)

      Person.update(personId, { $set: { "emails.0.address": email } })

      // TODO: Notify user that email address has been changed. Send enrollment email if password blank

    },
    /**
    * Updates user name
    * @param  {String} personId User record ID
    * @param  {String} name     User's name
    */
    'person.update.name'(personId, name) {
      check(personId, String)
      check(name, String)

      Person.update(personId, { $set: { "profile.name": name } })
    },
    /**
    * Updates user's team association
    * @param  {String} personId User record ID
    * @param  {String} teamId   Team record ID
    */
    'person.update.team'(personId, teamId) {
      check(personId, String)
      check(teamId, String)

      Person.update(personId, { $set: { "profile.team": teamId } })
    },
    /**
    * Updates user's hourly rate
    * @param  {String} personId User record ID
    * @param  {Float} rate   Hourly rate
    */
    'person.update.team'(personId, rate) {
      check(personId, String)
      check(rate, Number)

      Person.update(personId, { $set: { "profile.rate": rate } })
    },
    /**
    * Sets/removes user as Administrator
    * @param  {String}  personId User record ID
    * @param  {Boolean} isAdmin  Whether user is admin or not
    */
    'person.update.admin'(personId, isAdmin) {
      check(personId, String)
      check(isAdmin, Boolean)

      Person.update(personId, { $set: { "profile.isAdmin": isAdmin } })
    }
  })
}
