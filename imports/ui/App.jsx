import { Meteor } from 'meteor/meteor'
import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Header from './components/Header.jsx'
import Login from './Login.jsx'
import Password from './Password.jsx'

export default class App extends Component {
  render() {
    if (this.props.location.pathname.match(/enroll-account|reset-password/i)) {
      // Set password
      return(
        <Password token={this.props.params.token} {...this.props}/>
      )
    } else if (this.props.currentUser) {
      // Logged in
      return (
        <div>
          <Header />
          <div className="container">
            {this.props.children}
          </div>
        </div>
      )

    } else {
      // Show Login page
      return (
        <Login />
      )
    }
  }
}

export default createContainer(() => {
  return {
    currentUser: Meteor.user()
  }
}, App)
