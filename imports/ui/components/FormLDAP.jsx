import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { createContainer } from 'meteor/react-meteor-data'

export default class FormLDAP extends Component {
  constructor(props) {
    super(props)
    this.state = {ssl:false}
  }

  _submit(event) {
    event.preventDefault()
    const team = ReactDOM.findDOMNode(this.refs.name).value.trim()

    Team.insert({
      team,
      createdAt: new Date()
    })
    // Clear form
    ReactDOM.findDOMNode(this.refs.teamInput).value = ''
  }

  /**
  * Toggle administrator checkbox
  */
  _check() {
    this.setState({ssl:!this.state.ssl})
  }

  render() {
    return (
      <form onSubmit={this._submit.bind(this)}>
        <div className="row">
          <div className="input-field col s12 m4">
            <input id="hostname" ref="hostname" type="text" pattern="(http(s)*:\/\/)*[\w+\.]+\.\w+" className="validate" />
            <label for="hostname">Hostname</label>
          </div>
          <div className="input-field col s6 m2">
            <input id="port" ref="port" type="number" className="validate" />
            <label for="port">Port</label>
          </div>
          <div className="input-field col s6 m6" onClick={this._check.bind(this)}>
            <input type="checkbox" className="filled-in" id="ssl" checked={this.state.ssl} />
            <label for="ssl">Use SSL</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field col s12 m4">
            <input id="username" ref="username" type="text" />
            <label for="username">Username</label>
          </div>
          <div className="input-field col s12 m4">
            <input id="password" ref="password" type="password" className="validate"/>
            <label for="password">Password</label>
          </div>
          <div className="input-field col s12 m4">
            <input id="baseDN" ref="baseDN" type="text" />
            <label for="baseDN">Base DN</label>
          </div>
          <div className="input-field col s12 m4">
            <input id="userDN" ref="userDN" type="text" />
            <label for="userDN">User DN</label>
          </div>
          <div className="input-field col s12 m4">
            <input id="groupDN" ref="groupDN" type="text" />
            <label for="groupDN">Group DN</label>
          </div>
          <div className="input-field col s12 m4">
            {/* When test is successful, swap test button with connection button (use icon: cast_connected)*/}
            <a className="waves-effect waves-light btn right"><i className="material-icons left">cast</i>Test connection</a>
          </div>
        </div>
      </form>
    )
  }
}
