import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { createContainer } from 'meteor/react-meteor-data'

import { Team } from '../../api/team.js'
import Select from './Select.jsx'
import Message from './Message.jsx'

export default class FormLocal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isAdmin: false,
      error: null
    }
  }

  _submit(e) {
    e.preventDefault()
    if($('form')[0].checkValidity()) {
      this.setState({error: null})
      // All fields are valid
      const rate = ReactDOM.findDOMNode(this.refs.rate).value

      const newPerson = {
        name: ReactDOM.findDOMNode(this.refs.name).value.trim(),
        email: ReactDOM.findDOMNode(this.refs.email).value.trim(),
        team: ReactDOM.findDOMNode(this.refs.team).selected,
        rate: rate ? Number(rate) : undefined,
        isAdmin: ReactDOM.findDOMNode(this.refs.isAdmin).checked,
        source: 'Local'
      }
      Meteor.call('person.insert', newPerson, (error, result) => {
        if (error) {
          // Error writing to DB
          this.setState({error: error.error})
        } else {
          // Success! Clear form
          $('form')[0].reset()
          Materialize.updateTextFields()
        }
      })

    } else {
      this.setState({error: 'Provide a name and email.'})
    }
  }

  /**
  * Toggle administrator checkbox
  */
  _check() {
    this.setState({isAdmin:!this.state.isAdmin})
  }

  render() {
    const teams = [
      {id:'1', text:'Informatics'},
      {id:'2', text:'Lab'},
      {id:'3', text:'Clinical'}
    ]

    return (
      <form onSubmit={this._submit.bind(this)}>
        {this.state.error ? <Message>{this.state.error}</Message> : ''}
        <div className="row">
          <div className="input-field col s12 m4">
            <input id="name" ref="name" type="text" pattern="\w+[\w'\s-]+\w+" className="validate" required />
            <label for="name">Name</label>
          </div>
          <div className="input-field col s12 m4">
            <input id="name" ref="email" type="email" className="validate" required />
            <label for="name">Email</label>
          </div>
          <div className="input-field col s12 m4">
            <Select ref="team" data={teams} label="Team" />
          </div>
        </div>
        <div className="row">
          <div className="input-field col s12 m4">
            <i className="material-icons prefix">attach_money</i>
            <input id="rate" ref="rate" type="number" className="validate" step="0.01"/>
            <label for="name">Hourly Rate</label>
          </div>
          <div className="input-field col s6 m4" onClick={this._check.bind(this)}>
            <input type="checkbox" className="filled-in" id="isAdmin" ref="isAdmin" checked={this.state.isAdmin} />
            <label for="isAdmin">Administrator</label>
          </div>
          <div className="input-field col s6 m4">
            <button className="waves-effect waves-light btn right" type="submit" onClick={this._submit.bind(this)}><i className="material-icons left">person_add</i>Add</button>
          </div>
        </div>
      </form>
    )
  }
}

/**
* Reactive data
*/
export default createContainer(() => {
  return {
    teams: Team.find({}, { sort: { name: 1 } }).fetch()
  }
}, FormLocal)
