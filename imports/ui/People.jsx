import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { createContainer } from 'meteor/react-meteor-data'

import { Person } from '../api/person.js'
import { Team } from '../api/team.js'
import FormLocal from './components/FormLocal.jsx'
import FormLDAP from './components/FormLDAP.jsx'
import Message from './components/Message.jsx'
import Select from './components/Select.jsx'

export default class People extends Component {

  _updateAdmin(person) {
    console.log(person)
  }

  _renderCollection() {
    console.log(Person.find().fetch())
    return this.props.people.map((person) => (
      <tr key={person._id}>
        <td>{person.profile.name}</td>
        <td >{person.emails[0].address}</td>
        <td><Select data={this.props.teams} value={person.profile.team} label="Team" /></td>
        <td className="center-align" onClick={this._updateAdmin.bind(this, person)}>
          <input type="checkbox" className="filled-in" id="isAdmin" defaultChecked={true} />
          <label for="isAdmin"></label>
        </td>
        <td>{person.profile.rate ? `$${person.rate}/hr` : '—'}</td>
        <td>{person.profile.source}</td>
        <td><i className="material-icons">close</i></td>
      </tr>
    ))
  }

  render() {
    return (
      <div>
        <div className="card" >
          {/* Tabs */}
          <ul className="tabs">
            <li className="tab"><a href="#local">Local</a></li>
            <li className="tab"><a href="#ldap">Active Directory</a></li>
          </ul>
          <div className="row card-content">

            {/* Local */}
            <div id="local" className="col s12">
              <FormLocal />
            </div>

            {/* LDAP */}
            <div id="ldap" className="col s12">
              <FormLDAP />
            </div>
          </div>
        </div>

        {/* Existing teams */}
        <table className="bordered highlight">
          <thead>
            <tr>
              <th data-field="name">Name</th>
              <th data-field="email">Email</th>
              <th data-field="team">Team</th>
              <th data-field="role">Admin</th>
              <th data-field="rate">Rate</th>
              <th data-field="source">Source</th>
              <th></th>
            </tr>
          </thead>

          <tbody>
            {this._renderCollection()}
          </tbody>

        </table>
        {!this.props.people.length ? <Message>No people have been added yet!</Message> : ''}
      </div>
    )
  }
}

export default createContainer(() => {
  Meteor.subscribe('people')
  
  return {
    people: Person.find({}, { sort: { name: 1 } }).fetch(),
    teams: Team.find().fetch()
  }
}, People)
