import { Meteor } from 'meteor/meteor'
import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router'

import {TITLE} from '../constants.js'
import Message from './components/Message.jsx'

/**
 * Page to set password. Applies to
 * - Account enrollment
 * - Password reset
 */
export default class Password extends Component {
  constructor(props) {
    super(props)
    this.state = {invalid: false}
  }

  _submit(e) {
    e.preventDefault()
    if($('form')[0].checkValidity()) {
      // All fields are valid
      const password = ReactDOM.findDOMNode(this.refs.password).value.trim()
      const confirmation = ReactDOM.findDOMNode(this.refs.confirmation).value.trim()

      if (password === confirmation) {
        Accounts.resetPassword(this.props.token, password, (error) => {
          if (error) {
            // Password update failed
            this.setState({invalid: true})
          } else {
            // Password update successful - redirect
            browserHistory.push('/')
          }
        })

      } else {
        // Passwords do not match
        this.setState({invalid: true})
      }
    } else {
      // Invalid fields exist
      this.setState({invalid: true})
    }
  }

  render() {
    return (
      <div className="valign-wrapper row red lighten-2">
        <div className="valign col s10 m6 offset-s1 offset-m3 l4 offset-l4" >
          <h4 className="white-text">{TITLE} <i className="tiny material-icons">adb</i></h4>
          <div className="card">
            <form className="card-content">
              {this.state.invalid ? <Message>Passwords do no match</Message> : ''}
              <div className="row">
                <div className="input-field col s12">
                  <input id="password" ref="password" type="password" className="validate" required />
                  <label for="password">New password</label>
                </div>
                <div className="input-field col s12">
                  <input id="confirmation" ref="confirmation" type="password" className="validate" required />
                  <label for="confirmation">Confirm password</label>
                </div>
                <div className="input-field col s12">
                  <button className="waves-effect waves-light btn right" type="submit" onClick={this._submit.bind(this)}>Set password</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

Password.propTypes = {
  token: PropTypes.string.isRequired
}
