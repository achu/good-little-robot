import { Meteor } from 'meteor/meteor'
import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import {TITLE} from '../constants.js'
import Message from './components/Message.jsx'

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {invalid: false}
  }

  _submit(e) {
    e.preventDefault()
    if($('form')[0].checkValidity()) {
      // All fields are valid
      this.setState({invalid: false})
      Meteor.loginWithPassword(
        ReactDOM.findDOMNode(this.refs.email).value.trim(),
        ReactDOM.findDOMNode(this.refs.password).value.trim(),
        (err) => {
          if (err)
          this.setState({invalid: true})
        }
      )
    } else {
      // Invalid fields exist
      this.setState({invalid: true})
    }
  }

  render() {
    return (
      <div className="valign-wrapper row red lighten-2">
        <div className="valign col s10 m6 offset-s1 offset-m3 l4 offset-l4" >
          <h4 className="white-text">{TITLE} <i className="tiny material-icons">adb</i></h4>
          <div className="card">
            <form className="card-content">
              {this.state.invalid ? <Message>Email or password is invalid!</Message> : ''}
              <div className="row">
                <div className="input-field col s12">
                  <input id="name" ref="email" type="email" className="validate" required />
                  <label for="name">Email</label>
                </div>
                <div className="input-field col s12">
                  <input id="password" ref="password" type="password" className="validate" required />
                  <label for="password">Password</label>
                </div>
                <div className="input-field col s12">
                  <button className="waves-effect waves-light btn right" type="submit" onClick={this._submit.bind(this)}>Sign in</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
